import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { AccountService } from 'src/app/auth/account.service';
import { LoginService } from 'src/app/auth/login/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {


  user: string;

  constructor( private router: Router, 
    private authService: AuthService, 
    private accountService: AccountService,
    private loginService: LoginService ) { }

  ngOnInit(): void {
  
    this.getName();
  }

  
  public cerrar(){
    this.loginService.logout();
    this.router.navigate(['login']);
  }

  getName(){
    this.user = this.accountService.getUserName();
  }


}
