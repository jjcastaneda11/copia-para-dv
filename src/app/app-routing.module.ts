import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccesService } from './auth/user-route-acces.service';


const routes: Routes = [

  {
    path: 'dashboard',
    data:{
      authorities: [Authority.ADMIN, Authority.USER]
    },
    canActivate:[UserRouteAccesService],
    loadChildren: () => import('./dashboard/dashboard.module')
    .then(m => m.DashboardModule)
  },

  {
    path:'login',
    component:LoginComponent
  },

  {
    path:'access-denied',
    component:AccessDeniedComponent
  },

  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
