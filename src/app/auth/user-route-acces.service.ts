import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AccountService } from './account.service';
import { Observable, pipe } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserRouteAccesService implements CanActivate{

  constructor(
    private route: Router,
    private accountService: AccountService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    const authorities = route.data['authorities'];
    
    return this.checklogin(authorities, state.url);
  }

  checklogin(authorities: string[], url: string): Observable<boolean>{
    return this.accountService.identity()
    .pipe(map(account => {
      if (!authorities || authorities.length === 0){
        return true
      }

      if (account) {
        const hasAnyAuthority = this.accountService.hasAnyAuthority(authorities);

        if (hasAnyAuthority) {
          return true;
        }
        this.route.navigate(['access-denied']);
        return false;
      }
      this.route.navigate(['login']);
      return false;
      
  }));

  

  }

}
